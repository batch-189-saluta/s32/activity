let http = require('http')

let port = 4000

let server = http.createServer(function(request,response){
	if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile')
	} 
	else if (request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here's our courses")
	} 
	else if(request.url == '/addcourses' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add course to our resources')
	} 


	else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to booking system')
	}

	

})

server.listen(port)
console.log(`Server is running at a localhost: ${port}`)